# JGI Resources

<img style="float: right;" alt="Genepool logo"
src="../../img/Genepool-logo.jpg"> 

NERSC offers various resources for the
[DOE Joint Genome Institute](https://jgi.doe.gov/).

## [Computational Systems](systems.md)

JGI users have access to the main computational system at NERSC, Cori.
This access includes a JGI-exclusive partition named Genepool QOS and
a purpose-built large RAM node partition named ExVivo.

## [Filesystems](filesystems.md)

JGI users also have access to a variety of filesystems at NERSC, some of which
are shared and some of which are exclusive to JGI use.

## [Software](software.md)

NERSC provides general purpose and broad utility software via modules.
For specialized software we encourage users to build and install
their own as needed. This process can be streamlined by using one
of the software management systems available at NERSC.

## [Training and Tutorials](training.md)

This is a summary page for the training and tutorial sessions that have been
given at the JGI. Downloads of training materials are available.

## [Databases and Web Services](services.md)

NERSC provides web services and database hosting; this page provides
information about various requirements and how to get started.
