# Workflow nodes

We know that traditional supercomputing centers (including NERSC) are sometimes
not ideal for running long-lived scientific workflow tools. Batch jobs and
queue wait times can be obstacles to effectively using these tools.

We understand these difficulties! In our effort to better support workflows,
Cori has two dedicated nodes especially for running workflow software:
`cori20` and `cori21`.

## How can I get access to the Cori workflow nodes?

Please submit a ticket at help.nersc.gov

To help us decide if your use case is appropriate for the workflow nodes,
you will be asked:

```
User:
Email:
Repository Name:
Purpose:
Estimated memory usage:
Estimated CPU usage:
Estimated Data usage:
Estimated I/O:
Frequency / length of process:
Need external resources:
```

## Workflow node details

`cori20` and `cori21` are similar to the rest of the NERSC login nodes. They are Haswell nodes.
Once you have logged onto Cori, you can reach them via
```
ssh coriwork
```
A load balancer will then direct you to either `cori20` or `cori21`. From Cori, you can
also ssh directly to `cori20` or `cori21`.







